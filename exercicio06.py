# 1) Crie uma classe que represente um ônibus. O ônibus deverá conter os seguintes atributos:

# capacidade total
# capacidade atual
# movimento

# Os comportamentos esperados para um Ônibus são:
# Embarcar
# Desembarcar
# Acelerar
# Frear

# Lembre-se que a capacidade total do ônibus é de 45 pessoas - não será possível admitir super-
# lotação. Além disso, quando o ônibus ficar vazio, não será permitido efetuar o desembarque
# de pessoas. Além disso, pessoas não podem embarcar ou desembarcar com o onibus em movimento.

class Onibus:
    def __init__(self):
        self.capacidade_total = 2
        self.capacidade_atual = 0
        self.movimento = False

    def embarcar(self):
        if self.movimento == False and self.capacidade_atual < self.capacidade_total:
            self.capacidade_atual +=1
            return(f"Passageiro {self.capacidade_atual} embarcou com sucesso!")
        elif self.movimento == True:
            return("Onibus esta em movimento!")
        elif self.capacidade_atual >= self.capacidade_total:
            return(f"Capacidade maxima de {self.capacidade_total} passageiros ja foi atingida!")
        
    def desembarcar(self):
        if self.movimento == False and self.capacidade_atual > 0:
           self.capacidade_atual -=1
           return(f"Passageiro {self.capacidade_atual} desembarcou com sucesso!")
        elif self.movimento == True:
            return("Onibus esta em movimento!")
        elif self.capacidade_atual <= 0:
            return(f"O onibus esta vazio!")
    def acelerar(self):
        if self.movimento == True:
            return("Onibus ja esta em movimento!")
        else:
            self.movimento = True
            return("O onibus entrou em movimento!")
    def freiar(self):
        if self.movimento == False:
            return("Onibus ja esta em parado!")
        else:
            self.movimento = False
            return("O onibus esta freiando!")

cometa = Onibus()

# print(cometa.acelerar())
# print(cometa.embarcar())
# print(cometa.freiar())
# print(cometa.embarcar())
# print(cometa.embarcar())
# print(cometa.embarcar())
# print(cometa.acelerar())
# print(cometa.desembarcar())
# print(cometa.freiar())
# print(cometa.desembarcar())
# print(cometa.desembarcar())
# print(cometa.desembarcar())
# print(cometa.freiar())
# print(cometa.desembarcar())

# ==========================================================================
# 2) Implemente um programa que represente uma fila. O contexto do programa é uma
# agência de banco. Cada cliente ao chegar deverá respeitar a seguinte regra: o primeiro
# a chegar deverá ser o primeiro a sair. Você poderá representar pessoas na fila a par-
# tir de números os quais representam a idade. A sua fila deverá conter os seguintes
# comportamentos:

# • Adicionar pessoa na fila: adicionar uma pessoa na fila.
# • Atender Fila: atender a pessoa respeitando a ordem de chegada
# • Dar prioridade: colocar uma pessoa maior de 65 anos como o primeiro da fila

class Fila:
    def __init__(self):
        self.clientes_nome = list()
        self.clientes_idade = list()

    def adicionar_fila(self,nome,idade):
        self.clientes_nome.append(nome)
        self.clientes_idade.append(idade)
        return(self.clientes_nome, self.clientes_idade)
    
    def atender_fila(self):
        if len(self.clientes_nome) == 0:
            return("Nenhum cliente a ser atendido!")
        else:
            cliente = self.clientes_nome[0]
            self.clientes_nome.pop(0)
            self.clientes_idade.pop(0)
            return(f"Cliente {cliente} atendido(a)!")
    
    def checar_fila(self):
        if len(self.clientes_idade) == 0:
            return("Nao existe ninguem na fila!")
        else:
            return(self.clientes_nome, self.clientes_idade)
    
    def dar_prioridade(self):
        index = 0
        #print(len(self.clientes_idade))
        while index < len(self.clientes_idade):
            if self.clientes_idade[index] >= 65:
                nome_prioridade = self.clientes_nome[index]
                idade_prioridade = self.clientes_idade[index]
                self.clientes_nome.pop(index)
                self.clientes_idade.pop(index)
                self.clientes_nome.insert(0,nome_prioridade)
                self.clientes_idade.insert(0,idade_prioridade)
                index += 1
                return(f"Cliente {nome_prioridade} preferncial")
            #        break
            elif index >= len(self.clientes_idade)-1:
                index += 1
                #print(index)
                return("Nenhum cliente preferencial!")
            else:
                print(index)
                index += 1
            #break

banco = Fila()

print(banco.checar_fila())
print(banco.atender_fila())
print(banco.adicionar_fila("Matheus",29))
print(banco.adicionar_fila("Gabriel",25))
print(banco.adicionar_fila("Cida",93))
print(banco.atender_fila())
print(banco.checar_fila())
print(banco.dar_prioridade())
print(banco.checar_fila())

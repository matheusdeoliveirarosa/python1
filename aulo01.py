# Tipos Primitivos

# String    -> str      -> Frases, conjunto de caracteres, sempre entre aspas.
# Integer   -> int      -> Numeros inteiros, sem aspas
# Float     -> float    -> Numeros decimais, numeros rais, numeros com virgulas, sem aspas
# Boolean   -> bool     -> True or False, sem pastas.



# nome = "Matheus de Oliveira Rosa"   #String
# peso = "87"                         #String
# idade = 29                          #Integer
# altura = 1,80                       #Float
# moreno = True                       #Boolean


# #===========================================

# numero1 = 15
# numero2 = 25

# numero3 = numero1 + numero2
# #   print(numero3)

#=========================================== /#

nome = input("Qual o seu nome")
idade = int(input("Qual a sua idade"))

print(nome)
if idade>= 18:
    print("Maior de idade")
elif idade < 18 and idade >0:
    print("Menor de idade")
else:
    print("Idade invalida")

import random
import time
import csv
# 1) Escreva um programa em Python que simule uma dança das cadeiras. Você deverá
# importar o modulo random e iniciar uma lista com nomes de pessoas que participariam da
# brincadeira. O jogo deverá iniciar com 9 cadeiras e 10 participantes. A cada rodada,
# uma cadeira deverá ser retirada e um dos jogadores, de forma aleatória, ser eliminado. O
# jogo deverá terminar quando apenas restar uma cadeira e ao final de todas as rodadas,
# deverá ser apresentado vencedor.

# Dica: [OPCIONAL] Você poderá utilizar o modulo "time" para simular um tempo de a cada rodada para criar
# um efeito mais interessante.

# Dica: [OPCIONAL] Tentem fazer a remoção de uma pessoa aleatória por rodada sem utilizar a função "choice".

#*******************************************

participantes = ["Matheus", "Gabriel", "Carlos", "Tiago" , "Jose", "Lucas", "Bruno", "Marcos", "Breno", "Davi"]

def danca_das_cadeiras(participantes):
    
    rodadas = len(participantes)

    while rodadas >= 2:
        numerodeparticipantes = len(participantes)
        eliminado = random.randrange(0,numerodeparticipantes)
        print(f"O eliminado foi: {participantes[eliminado]}")
        participantes.pop(eliminado)
        #print(len(participantes))
        rodadas = len(participantes)
        wait = random.randrange(1,numerodeparticipantes)
        time.sleep(wait)
    print(f"O Vencedor foi: {participantes[0]}")        
# danca_das_cadeiras(participantes)



# =============================================================
# 2) Crie um programa que pergunte para o usuario um numero de pessoas a participarem de um sorteio (2-20),
#  e o numero de pessoas a serem sorteadas, e depois sorteie esse numero de pessoas da lista.

# O programa deverá pegar o numero de pessoas a participar aleatoriamente desta lista:

lista_de_participantes = ["Joao", "Maria", "Tiago", "Amanda", "Emanuele", "Caio", "Suzana", "Miguel", 
"Rosangela", "Rian", "Lucimar", "Ulisses", "Leonardo", "Kaique", "Bruno", "Raquel", 
"Benedito", "Tereza", "Valmir", "Joaquim"]

def sorteio(lista):
    participantes = []
    numero_de_sorteios = 0
    i=0
    numero_de_participantes= int(input("Digite o numero de participantes de 1 a 20: "))
    while numero_de_sorteios == 0 or numero_de_sorteios > numero_de_participantes:
        numero_de_sorteios = int(input("Digite o numero de pessoas a serem sorteadas: "))
        if numero_de_sorteios > numero_de_participantes:
            print("Numero de sorteios maior que numero de participantes!")
    for loop in range (0, numero_de_participantes):
        escolidos = random.choice(lista)
        participantes.append(escolidos)
        lista.remove(escolidos)
    print(f"Os participantes serao: \n{participantes}")

    for sorteios in range (0, numero_de_sorteios):
        ganhador = random.choice(participantes)
        i += 1
        print(f"{ganhador} venceu o sorteio {i}")
        participantes.remove(ganhador)

#sorteio(lista_de_participantes)


# Nota: A mesma pessoa não pode ganhar duas vezes.


# =============================================================
# 3) Escreva um programa em python que conte as vogais que a música ‘Faroeste Caboclo’
# tem em sua letra. Armazena a letra da música em um arquivo do tipo txt.
# Dica: Não se esqueça de considerar as letras maiúsculas, minúsculas e com acentuação.

def contador_de_vogais():
    vogal = 0

    with open("faroeste.txt","r") as letra:
        linhas = letra.readlines()
    for total_linhas in range(0,len(linhas)):
        linha = linhas[total_linhas]
        for letra in linha:
            if letra == "A" or letra == "Á" or letra == "À" or letra == "Â" or letra == "Ã" \
            or letra == "E" or letra == "É" or letra == "Ê" or letra == "I" or letra == "Í" \
            or letra == "Î" or letra == "O" or letra == "Ó" or letra == "Ô" or letra == "Õ" \
            or letra == "U" or letra == "Ú" or letra == "a" or letra == "á" or letra == "à" \
            or letra == "â" or letra == "ã" or letra == "e" or letra == "é" or letra == "ê" \
            or letra == "i" or letra == "í" or letra == "î" or letra == "o" or letra == "ó" \
            or letra == "ô" or letra == "õ" or letra == "u" or letra == "ú":
                vogal += 1
    print(vogal)

#contador_de_vogais()

# ====================================================================================
# 4) Escreva um programa em python que realize um cadastro. Deverão ser coletadas as
# seguintes informações:

# CPF
# Nome
# Idade
# Sexo
# Endereço
def cadastro():
    while True:
        opcao = input("Gostaria de cadastrar uma pessoa nova no banco de dados? [S/N]")

        if opcao.upper() == "N":
            break

        elif opcao.upper() == "S":
            cpf = input("Qual é o seu CPF? ")
            nome = input("Qual é o seu nome? ")
            idade = input("Qual é a sua idade? ")
            sexo = input("Qual é o seu sexo? ")
            address = input("Qual é o seu endereço? ")  

            lista_nomes = (cpf, nome, idade, sexo, address)

            with open("cadastro.csv", "a") as arquivo:
                conteudo = csv.writer(arquivo, delimiter=";")
                conteudo.writerow(lista_nomes)

        else:
            print("Opção inválida")


#cadastro()

    
# Os registros deverão ser armazenados em um arquivo CSV. Caso desejar manter o padrão
# brasileiro, o CSV será separado pelo caractere ;

def saudacao(nome):
    print(f"Seja bem vindo {nome}!")

def calculadora(numero1, numero2):
    operacao = int(input(""" Selecione a opcao:
1 - Soma
2 - Subtracao
3 - Multiplicacao
4 - Divisao
"""))
    if operacao == 1:
        print(f"Soma de {numero1} e {numero2} igual a {numero1+numero2}")
    elif operacao == 2:
        print(f"Subtracao de {numero1} e {numero2} igual a {numero1-numero2}")
    elif operacao == 3:
        print(f"Multiplicacao de {numero1} e {numero2} igual a {numero1*numero2}")
    elif operacao == 4:
        print(f"Divisao de {numero1} e {numero2} igual a {numero1/numero2}")
    else:
        print("Opcao invalida")

def numeros_pares(*valores):
    pares=0
    for x in valores:
        if x % 2 == 0:
            pares += 1
    print(f"Numeros pares igual a {pares}")
    
def latas_tinta(altura, largura):
    metrosquadrados = altura * largura
    if metrosquadrados % 3 != 0:
        lata = (metrosquadrados // 3) + 1
    else:
        lata = metrosquadrados / 3
    print(f"Para pintar a parede de {metrosquadrados} metros quadrados seram necessarias {lata:.0f} latas")

#saudacao(input("Digite o seu nome:"))

#calculadora(float(input("Digite o primeiro numero: ")),float(input("Digite o segundo numero: ")))

#numeros_pares(20,30,15,77)

#latas_tinta(float(input("Digite a altura da parede: ")), float(input("Digite a largura da parede: ")))




# # Persistencia de dados com OPEN

# # Modos:
# # w - write  - escrever
# # r - read   - ler
# # a - append - adicionar


nome = input("Qual o seu nome? ")

arquivo = open("nomes.txt", "a")
arquivo.write(f"{nome}\n")
arquivo.close()

arquivo = open("nomes.txt", "r")
conteudo = arquivo.read()
print(conteudo)
arquivo.close()

with open("nomes.txt", "a") as arquivo:
    arquivo.write("Testando o with open")

# ============================================
# csv
import csv

with open("planilha.csv", "r") as arquivo:
    conteudo = csv.reader(arquivo, delimiter=";")

    #Generator
    for linha in conteudo:
        print(linha)

# Tipos Primitivos

# String  -> str   -> Frases, conjunto de caracteres, sempre entre aspas.
# Integer -> int   -> Numeros inteiros, sem aspas.
# Float   -> float -> Numeros decimais, numeros reais, numeros com ponto, sem aspas.
# Boolean -> bool  -> True ou False, sem pastas.

# nome = "tiago P. Lima"  # String
# peso = "80"             # String
# idade = 27              # Integer
# altura = 1.80           # Float
# moreno = True           # Boolean

# #=======================================================

numero1 = 15
numero2 = 25

numero3 = numero1 + numero2
print(numero3)

# =====================================================

idade = input("Qual o sua idade? ")

idade = int(idade)

print(idade)

# ====================================================

nome = "tiago p. lima".replace("p", "d")

print(nome)
print(nome.islower())

# ====================================================

#Estruturas de decisão

idade = int(input("Qual a sua idade? "))

if idade > 17:
    print("Voce pode entrar e comprar bebidas no bar.")
    print("Aproveite a noite")

elif idade > 15:
    print("Voce pode entrar, mas não pode comprar bebidas no bar.")

else:
    print("Voce é muito jovem pra entrar.")

# ===========================================
    
# Estruturas de repetição
    
# FOR e WHILE
    
contador = 10
while contador != 0:
    print("Estou contando...")
    print("Executando codigo...")
    contador = contador - 1
    contador = int(input("Digite um numero: "))

while True:
    resposta = input("Quer parar o programa? [s/n]: ")
    if resposta == "s":
        break

    resposta2 = input("Qur continuar a execução atual? [s/n]: ")
    if resposta2 == "s":
        continue

    print()
    print()

    while True:
        print("teste")
        break

# ===============================
    
for x in range(0, 10):
    print(f"Imprimindo a {x}ª mensagem")

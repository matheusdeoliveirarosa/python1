import numpy as np
matrix = np.zeros((3,3))
print(matrix)

print(f'''
    1     2     3
    {matrix[0][0]:.0f}  |     |
1 _____|_____|_____
       |     |
2 _____|_____|_____
       |     |
3      |     |
''')